import org.common.Repo

def call() {
    def repos = [
            new Repo(name: "white-label-emails"),
            new Repo(name: "mg"),
            new Repo(name: "consumer"),
            new Repo(name: "i-portal"),
            new Repo(name: "mg-pro"),
            new Repo(name: "landing-site"),
            new Repo(name: "white-label-statics"),
            new Repo(name: "e2e"),
            new Repo(name: "ansible")
        ]
    return repos
}
