package org.common;

class Repo {
    String name
    String latestTag
    boolean hasChanges = false
    boolean toRelease = false
    Change[] changes = []
}
